package com.person;

import java.util.Objects;

public class Person {
  private String name;
  private String surname;
  private String city;

  public Person(String name, String surname, String city) {
    this.name = name;
    this.surname = surname;
    this.city = city;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return this.surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getCity() {
    return this.city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this)
      return true;
    if (!(o instanceof Person)) {
      return false;
    }
    Person person = (Person) o;
    return Objects.equals(name, person.name) && Objects.equals(surname, person.surname)
        && Objects.equals(city, person.city);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, surname, city);
  }

  @Override
  public String toString() {
    return "{" +
        " name='" + getName() + "'" +
        ", surname='" + getSurname() + "'" +
        ", city='" + getCity() + "'" +
        "}";
  }

}
