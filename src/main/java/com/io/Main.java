package com.io;

import com.person.Person;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    System.out.println("Start");
    // Reading file & Extract data
    try {
      String pathPersons = "/home/kyabe/EPSI/Java/io/assets/persons.txt";
      System.out.println("Collection Creation");
      List<Person> personsList = returnList(pathPersons);
      String outputString = "/home/kyabe/EPSI/Java/io/output/personnes.html";
      System.out.println("HTML Creation");
      createHTML(personsList, outputString);
    } catch (FileNotFoundException e) {
      // Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // Auto-generated catch block
      e.printStackTrace();
    }
  }

  private static void createHTML(List<Person> personsList, String outputString) throws FileNotFoundException {
    PrintWriter pWriter = new PrintWriter(outputString);
    pWriter.println("<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" crossorigin=\"anonymous\">");

    pWriter.println("<table class=\"table\">\n" +
        "<tr>" +
        "<th class=\"table-light\">Prénom</th>" +
        "<th class=\"table-light\">Nom</th>" +
        "<th class=\"table-light\">Ville</th>" +
        "</tr>");
        
    personsList.forEach(p -> {
      pWriter.println("<tr>" +
          "<td>" + p.getName() + "</td>" +
          "<td>" + p.getSurname() + "</td>" +
          "<td>" + p.getCity() + "</td>" +
          "</tr>");
    });
    pWriter.println("</table>");

    pWriter.close();

  }

  private static List<Person> returnList(String pathPersons) throws FileNotFoundException, IOException {
    BufferedReader buffer = new BufferedReader(new FileReader(pathPersons));
    List<Person> ps = new ArrayList<>();
    while (buffer.ready()) {
      String line = buffer.readLine();
      String[] tab = line.split(";");
      ps.add(new Person(tab[0], tab[1], tab[2]));
    }
    buffer.close();
    return ps;
  }
}
